/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa.calculatorfeature;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    private Stack<BigDecimal> dStack = new Stack<BigDecimal>();
    private Stack<QueryElement> nStack = new Stack<QueryElement>();
    private BigDecimal calculationResult;

    private Locale locale;

    public Calculator(Locale locale) {
        this.locale = locale;
    }

    public BigDecimal calculate(String query) throws QueryElementException {
//        NumberFormat.getNumberInstance(Locale.FRANCE).parse("265,858")
        List<QueryElement> listQuery = convertQuery(query);
        for (QueryElement qe : listQuery) {
            if (qe.isOperation()) {
                if (nStack.empty()) {
                    nStack.push(qe);
                } else {
                    if (nStack.peek().getOperationType().getPriority() <= qe.getOperationType().getPriority()) {
                        makeOperation(nStack.pop());
                    }
                    nStack.push(qe);
                }
            } else {
                dStack.push(qe.getValue());
            }
        }
        return finishCalculation();
    }

    private BigDecimal finishCalculation() throws QueryElementException {
        while (true) {
            if (!dStack.empty()) {
                if (nStack.empty()) {
                    return dStack.pop();
                }
                makeOperation(nStack.pop());
            } else {
                break;
            }
        }
        return calculationResult;
    }

    private void makeOperation(QueryElement operation) throws QueryElementException {
        BigDecimal secondOperand = dStack.pop();
        if (dStack.empty()) {
            calculationResult = secondOperand;
            return;
        }
        BigDecimal firstOperand = dStack.pop();
        String res = null;
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.setMaximumFractionDigits(20);
        df.setMinimumFractionDigits(0);
        switch (operation.getOperationType().getValue()) {
            case OperationCodes.DIVIDE:
                if (secondOperand.compareTo(new BigDecimal("0")) == 0) {
                    throw new QueryElementException("Cannot divide by zero");
                }
                res = df.format(firstOperand.divide(secondOperand, 20, BigDecimal.ROUND_HALF_DOWN));
                break;
            case OperationCodes.MINUS:
                res = df.format(firstOperand.subtract(secondOperand));
                break;
            case OperationCodes.MULTIPLY:
                res = df.format(firstOperand.multiply(secondOperand));
                break;
            case OperationCodes.PLUS:
                res = df.format(firstOperand.add(secondOperand));
                break;
            default:
                break;
        }
        if (res != null) {
            res = res.replaceAll(",", "");
            dStack.push(new BigDecimal(res));
        }
    }

    public List<QueryElement> convertQuery(String query) {
        List<QueryElement> res = new ArrayList<QueryElement>();
        StringBuilder tempValue = new StringBuilder();
        for (int x = 0; x < query.length(); x++) {
            if (query.charAt(x) == '+') {
                if (tempValue.charAt(tempValue.length() - 1) == 'E') {
                    tempValue.append(query.charAt(x));
                    continue;
                }
                res.add(new QueryElement(tempValue.toString(), OperationType.NONE));
                tempValue = new StringBuilder();
                res.add(new QueryElement(null, OperationType.PLUS));
                continue;
            }
            if (query.charAt(x) == '-') {
                if (tempValue.charAt(tempValue.length() - 1) == 'E') {
                    tempValue.append(query.charAt(x));
                    continue;
                }
                res.add(new QueryElement(tempValue.toString(), OperationType.NONE));
                tempValue = new StringBuilder();
                res.add(new QueryElement(null, OperationType.MINUS));
                continue;
            }
            if (query.charAt(x) == '*') {
                res.add(new QueryElement(tempValue.toString(), OperationType.NONE));
                tempValue = new StringBuilder();
                res.add(new QueryElement(null, OperationType.MULTIPLY));
                continue;
            }
            if (query.charAt(x) == '/') {
                res.add(new QueryElement(tempValue.toString(), OperationType.NONE));
                tempValue = new StringBuilder();
                res.add(new QueryElement(null, OperationType.DIVIDE));
                continue;
            }
            tempValue.append(query.charAt(x));
        }
        if (tempValue.length() == 0) {
            res.remove(res.size() - 1);
        } else {
            res.add(new QueryElement(tempValue.toString(), OperationType.NONE));
        }
        return res;
    }

    public static void main(String[] args) {
        BigDecimal b = new BigDecimal("1,2");
        System.out.println(b.toString());
    }
//        String format = "Unexpected unhandled exception. Details: %s";
//        System.out.println(String.format(format, "123", "321", "231"));
//        final String activityGUID = (String) Context.peek().getGlobal().get(Fields.ActivityGUID.name());
//    }

}
