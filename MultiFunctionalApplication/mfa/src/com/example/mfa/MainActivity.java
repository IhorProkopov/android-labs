package com.example.mfa;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import com.example.mfa.calculatorfeature.CalculatorActivity;
import com.example.mfa.colourfeature.ColourActivity;
import com.example.mfa.mapfeature.MapActivity;
import com.example.mfa.notesfeature.NotesActivity;
import com.example.mfa.playerfeature.PlayerActivity;

public class MainActivity extends Activity implements View.OnClickListener {

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.main);
        Spinner styleSpinner = (Spinner) findViewById(R.id.style_spinner);
        styleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ActivityStyle activityStyle = ActivityStyle.getInstance();
                if (position == 0) {
                    activityStyle.setPathToTTF("fonts/1942.ttf");
                    activityStyle.setBackgroungID(R.drawable.background1);
                } else {
                    if (position == 1) {
                        activityStyle.setPathToTTF("fonts/3Dumb.ttf");
                        activityStyle.setBackgroungID(R.drawable.background2);
                    } else {
                        activityStyle.setPathToTTF(null);
                        activityStyle.setBackgroungID(R.drawable.bacground3);
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
    }

    public void openColourPanel(View view) {
        Intent myIntent = new Intent(MainActivity.this, ColourActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    public void openCalculator(View view) {
        Intent myIntent = new Intent(MainActivity.this, CalculatorActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    public void openNotes(View view) {
        Intent myIntent = new Intent(MainActivity.this, NotesActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    public void openPlayer(View view) {
        Intent myIntent = new Intent(MainActivity.this, PlayerActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    public void openMap(View view) {
        Intent myIntent = new Intent(MainActivity.this, MapActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    @Override
    public void onClick(View v) {

    }
}