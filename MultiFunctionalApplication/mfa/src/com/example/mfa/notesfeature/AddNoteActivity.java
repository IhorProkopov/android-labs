/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa.notesfeature;

import android.app.*;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.mfa.ActivityStyle;
import com.example.mfa.ActivityUtils;
import com.example.mfa.R;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class AddNoteActivity extends Activity implements View.OnClickListener {

    public AlarmManager alarmManager;
    Intent alarmIntent;
    PendingIntent pendingIntent;
    private NotesDAO notesDAO;

    private EditText description;
    private EditText value;
    private Spinner importanceSpinner;

    private static final int DIALOG_DATE = 1;
    private static final int DIALOG_TIME = 2;
    private static final int SELECT_PICTURE = 3;
    private static final int SELECT_PICTURE2 = 4;
    private Calendar calendar = Calendar.getInstance();
    private String imagePath, imagePath2;
    private static boolean isCreating;
    private static int noteID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notes_adding_page);
        notesDAO = new NotesDAO(this);
        description = (EditText) findViewById(R.id.descriptionEditText);
        value = (EditText) findViewById(R.id.valueEditText);
        importanceSpinner = (Spinner) findViewById(R.id.importanceSpinner);
        Button dateButton = (Button) findViewById(R.id.showNoteDatePicker);
        Button timeButton = (Button) findViewById(R.id.showNoteTimePicker);
        Button okButton = (Button) findViewById(R.id.saveNoteButton);
        Button imageButton = (Button) findViewById(R.id.getImageButton);
        Button imageButton2 = (Button) findViewById(R.id.setImage2);
        imageButton2.setOnClickListener(this);
        timeButton.setOnClickListener(this);
        dateButton.setOnClickListener(this);
        okButton.setOnClickListener(this);
        imageButton.setOnClickListener(this);

        isCreating = getIntent().getExtras().getBoolean("isCreating", true);
        noteID = getIntent().getExtras().getInt("noteID", 0);
        if (!isCreating) {
            Note note = notesDAO.getNote(noteID);
            value.setText(note.getValue());
            description.setText(note.getDescription());
            calendar = new GregorianCalendar();
            calendar.setTime(note.getLastEditDate());
            imagePath = note.getImagePath();
            imagePath2 = note.getImagePath2();
            importanceSpinner.setSelection(((ArrayAdapter) importanceSpinner.getAdapter()).getPosition(note.getImportance().getValue()));
        }
        ViewGroup layout = (ViewGroup) findViewById(R.id.add_notes_root_layout);
        ActivityUtils.setStyleToTextView(ActivityStyle.getInstance(), layout, getAssets());
        if (ActivityStyle.getInstance().getBackgroungID() != 0)
            layout.setBackgroundResource(ActivityStyle.getInstance().getBackgroungID());
    }

    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_DATE) {
            return new DatePickerDialog(this, dateCallBAck, 2014, 11, 25);
        }
        if (id == DIALOG_TIME) {
            return new TimePickerDialog(this, timeCallBack, 12, 30, true);
        }
        return super.onCreateDialog(id);
    }

    DatePickerDialog.OnDateSetListener dateCallBAck = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar.set(year, monthOfYear, dayOfMonth);
        }
    };

    TimePickerDialog.OnTimeSetListener timeCallBack = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            calendar.set(Calendar.MINUTE, minute);
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        }
    };

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.showNoteDatePicker) {
            showDialog(DIALOG_DATE);
        }
        if (v.getId() == R.id.showNoteTimePicker) {
            showDialog(DIALOG_TIME);
        }
        if (v.getId() == R.id.getImageButton) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,
                    "Select Picture"), SELECT_PICTURE);
        }
        if (v.getId() == R.id.setImage2) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,
                    "Select Picture"), SELECT_PICTURE2);
        }
        if (v.getId() == R.id.saveNoteButton) {
            importanceSpinner = (Spinner) findViewById(R.id.importanceSpinner);
            Note note = initNote(description.getText().toString(), value.getText().toString(), String.valueOf(importanceSpinner.getSelectedItem()));
            if (isCreating) {
                notesDAO.add(note);
            } else {
                notesDAO.update(note);
            }
            setNotification(note);
            finish();
        }

    }

    private Note initNote(String description, String value, String importance) {
        Note note = new Note();
        note.setId(noteID);
        note.setDescription(description);
        note.setValue(value);
        if (importance.equals("Not important")) {
            note.setImportance(Importance.NOT_IMPORTANT);
        }
        if (importance.equals("Important")) {
            note.setImportance(Importance.IMPORTANT);
        }
        if (importance.equals("Very Important")) {
            note.setImportance(Importance.VERY_IMPORTANT);
        }
        note.setLastEditDate(calendar.getTime());
        note.setImagePath(imagePath);
        note.setImagePath2(imagePath2);
        return note;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && data != null) {
            String realPath;
            // SDK < API11
            if (Build.VERSION.SDK_INT < 11)
                realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());

                // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19)
                realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());

                // SDK > 19 (Android 4.4)
            else
                realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());

            if (requestCode == SELECT_PICTURE)
                imagePath = realPath;
            else {

                imagePath2 = realPath;

            }
        }

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void setNotification(Note note) {
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmIntent = new Intent(AddNoteActivity.this, AlarmReceiver.class);
        alarmIntent.putExtra("noteID", note.getId());
        pendingIntent = PendingIntent.getBroadcast(AddNoteActivity.this, 0, alarmIntent, 0);
        Calendar alarmStartTime = Calendar.getInstance();
        alarmStartTime.add(Calendar.MINUTE, 2);
        alarmManager.set(AlarmManager.RTC_WAKEUP, note.getLastEditDate().getTime(), pendingIntent);
        Log.i("Notification", "Add note setNotification!");
    }

}
