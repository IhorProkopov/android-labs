/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa.calculatorfeature;

public enum OperationType {

    PLUS(0, 1), MINUS(1, 1), MULTIPLY(2, 0), DIVIDE(3, 0), NONE(4, -1);

    public int getValue() {
        return value;
    }

    private OperationType(int value, int priority) {
        this.value = value;
        this.priority = priority;
    }

    private int value;

    public int getPriority() {
        return priority;
    }

    private int priority;


}
