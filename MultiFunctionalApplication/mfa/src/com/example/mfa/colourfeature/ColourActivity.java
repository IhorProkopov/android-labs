/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa.colourfeature;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.SeekBar;
import com.example.mfa.R;

public class ColourActivity extends Activity implements SeekBar.OnSeekBarChangeListener {

    private ImageView imageView;
    private SeekBar redSeekBar;
    private SeekBar blueSeekBar;
    private SeekBar greenSeekBar;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.colour_page);

        imageView = (ImageView) findViewById(R.id.imageView);
        redSeekBar = (SeekBar) findViewById(R.id.blueSeekBar);
        blueSeekBar = (SeekBar) findViewById(R.id.greenSeekBar);
        greenSeekBar = (SeekBar) findViewById(R.id.redSeekBar);
        redSeekBar.setOnSeekBarChangeListener(this);
        blueSeekBar.setOnSeekBarChangeListener(this);
        greenSeekBar.setOnSeekBarChangeListener(this);
        changeBackGround();

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress,
                                  boolean fromUser) {
        changeBackGround();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private void changeBackGround() {
        int redColour = redSeekBar.getProgress();
        int blueColour = blueSeekBar.getProgress();
        int greenColour = greenSeekBar.getProgress();
        imageView.setBackgroundColor(Color.rgb(redColour, greenColour, blueColour));
    }

}
