/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa.notesfeature;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

class DBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "notesDB";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.v("Log", "--- onCreate database ---");
        db.execSQL("create table IF NOT EXISTS notes ("
                + "id integer primary key autoincrement,"
                + "value text,"
                + "description text,"
                + "lastEditDate text,"
                + "imagePath text,"
                + "imagePath2 text,"
                + "importance text" + ");");
//        db.execSQL("ALTER TABLE notes " +
//                "ADD imagePath text");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
