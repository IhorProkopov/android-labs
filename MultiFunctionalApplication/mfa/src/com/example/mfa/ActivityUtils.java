/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa;

import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ActivityUtils {
    public static void setStyleToTextView(ActivityStyle style, ViewGroup viewGroup, AssetManager assetManager) {
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View v = viewGroup.getChildAt(i);
            if (v instanceof TextView) {
                TextView n = (TextView) v;

                if (style.getPathToTTF() == null) {
                    n.setTextColor(Color.BLACK);
                } else {
                    Typeface myTypeface = Typeface.createFromAsset(assetManager, style.getPathToTTF());
                    n.setTypeface(myTypeface);
                }
            } else {
                if (v instanceof ViewGroup) {
                    setStyleToTextView(style, (ViewGroup) v, assetManager);
                }
            }
        }
    }
}