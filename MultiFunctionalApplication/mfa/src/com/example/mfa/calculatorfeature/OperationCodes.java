package com.example.mfa.calculatorfeature;

public class OperationCodes {
    public static final int PLUS = 0;
    public static final int MINUS = 1;
    public static final int MULTIPLY = 2;
    public static final int DIVIDE = 3;

}
