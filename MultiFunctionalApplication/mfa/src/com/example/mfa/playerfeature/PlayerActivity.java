package com.example.mfa.playerfeature;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import com.example.mfa.R;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class PlayerActivity extends Activity implements View.OnClickListener, MediaPlayer.OnCompletionListener, SeekBar.OnSeekBarChangeListener {
    private static final int PLAY_LIST_ACTIVITY = 100;
    private boolean isPlaying = false;
    private boolean isPaused = false;
    private List<HashMap<String, String>> musics = MediaUtil.getPlayList();
    private MediaPlayer mp;
    private TextView songTitleLabel;
    private int currentSongIndex = 0;
    private ImageButton playButton, repeatButton, shuffleButton;
    private SeekBar songProgressBar;
    private Handler mHandler = new Handler();
    private TextView songTotalDurationLabel, songCurrentDurationLabel;
    private boolean isShuffled, isRepeat = false;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.madia_page);
        playButton = (ImageButton) findViewById(R.id.btnPlay);
        playButton.setOnClickListener(this);
        ImageButton previousButton = (ImageButton) findViewById(R.id.btnPrevious);
        previousButton.setOnClickListener(this);
        ImageButton nextButton = (ImageButton) findViewById(R.id.btnNext);
        nextButton.setOnClickListener(this);
        songTitleLabel = (TextView) findViewById(R.id.songTitle);
        mp = new MediaPlayer();
        ImageButton btnPlaylist = (ImageButton) findViewById(R.id.btnPlaylist);
        btnPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), PlayListActivity.class);
                startActivityForResult(i, PLAY_LIST_ACTIVITY);
            }
        });
        repeatButton = (ImageButton) findViewById(R.id.btnRepeat);
        repeatButton.setOnClickListener(this);
        shuffleButton = (ImageButton) findViewById(R.id.btnShuffle);
        shuffleButton.setOnClickListener(this);
        songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
        songProgressBar.setOnSeekBarChangeListener(this);
        mp.setOnCompletionListener(this);
        songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == PLAY_LIST_ACTIVITY) {
            currentSongIndex = data.getExtras().getInt("songIndex");
            // play selected song
            playSong(currentSongIndex);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPlay:
                if (!isPlaying) {
                    playButton.setBackgroundResource(R.drawable.pause_button);
                    playSong(currentSongIndex);
                } else {
                    playButton.setBackgroundResource(R.drawable.play_btn);
                    stopPlaying();
                }
                break;
            case R.id.btnNext:
                currentSongIndex++;
                playSong(currentSongIndex);
                break;
            case R.id.btnPrevious:
                currentSongIndex--;
                playSong(currentSongIndex);
                break;
            case R.id.btnRepeat:
                if (isRepeat) {
                    repeatButton.setBackgroundResource(R.drawable.repeat);
                } else {
                    repeatButton.setBackgroundResource(R.drawable.repeat_enabled);
                }
                isRepeat = !isRepeat;
                break;
            case R.id.btnShuffle:
                if (isShuffled) {
                    shuffleButton.setBackgroundResource(R.drawable.shuffle);
                } else {
                    shuffleButton.setBackgroundResource(R.drawable.shuffle_enabled);
                }
                isShuffled = !isShuffled;
                break;
        }
    }

    private void stopPlaying() {
        isPlaying = false;
        isPaused = true;
        mp.pause();
    }

    private void playSong(int songIndex) {
        isPlaying = true;
        if(isPaused){
            mp.start();
            return;
        }
        // Play song
        try {
            mp.reset();
            mp.setDataSource(musics.get(songIndex).get("songPath"));
            mp.prepare();
            mp.start();
            // Displaying Song title
            String songTitle = musics.get(songIndex).get("songTitle");
            songTitleLabel.setText(songTitle);


        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (!isPlaying) {
            return;
        }
        if (isRepeat) {
            playSong(currentSongIndex);
        } else {
            if (isShuffled) {
                currentSongIndex = new Random().nextInt((musics.size() - 1));
                playSong(currentSongIndex);
            } else {
                if (currentSongIndex == musics.size() - 1) {
                    currentSongIndex = -1;
                }
                currentSongIndex++;
                playSong(currentSongIndex);
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = mp.getDuration();
            long currentDuration = mp.getCurrentPosition();

            // Displaying Total Duration time
            songTotalDurationLabel.setText("" + MediaUtil.milliSecondsToTimer(totalDuration));
            // Displaying time completed playing
            songCurrentDurationLabel.setText("" + MediaUtil.milliSecondsToTimer(currentDuration));

            // Updating progress bar
            int progress = (int) (MediaUtil.getProgressPercentage(currentDuration, totalDuration));
            //Log.d("Progress", ""+progress);
            songProgressBar.setProgress(progress);

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = mp.getDuration();
        int currentPosition = MediaUtil.progressToTimer(seekBar.getProgress(), totalDuration);


        mp.seekTo(currentPosition);

        updateProgressBar();
    }

    PhoneStateListener phoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            if (state == TelephonyManager.CALL_STATE_RINGING) {
                mp.pause();
            } else if(state == TelephonyManager.CALL_STATE_IDLE) {
                mp.start();
            } else if(state == TelephonyManager.CALL_STATE_OFFHOOK) {
                //A call is dialing, active or on hold
            }
            super.onCallStateChanged(state, incomingNumber);
        }
    };

}
