package com.example.mfa.mapfeature;

import android.app.Activity;
import android.os.Bundle;
import com.example.mfa.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.*;


public class MapActivity extends Activity {

    static final LatLng HAMBURG = new LatLng(53.558, 9.927);
    static final LatLng KIEL = new LatLng(53.551, 9.993);
    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_page);
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                .getMap();
        Marker hamburg = map.addMarker(new MarkerOptions().position(HAMBURG)
                .title("Hamburg"));
        Marker kiel = map.addMarker(new MarkerOptions()
                .position(KIEL)
                .title("Kiel")
                .snippet("Kiel is cool")
                .icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.ic_launcher)));
        PolylineOptions rectOptions = new PolylineOptions()
                .add(KIEL)
                .add(HAMBURG);
        map.addPolyline(rectOptions);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(HAMBURG, 15));


        map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
    }


}
