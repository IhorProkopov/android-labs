/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa.calculatorfeature;


import java.math.BigDecimal;

public class QueryElement {

    private BigDecimal value;

    private OperationType operationType;


    public QueryElement() {
    }

    public QueryElement(String value, OperationType operationType) {
        this.operationType = operationType;
        if (operationType.equals(OperationType.NONE)) {
            if (value.equals(".")) {
                this.value = new BigDecimal("0");
            } else {
                this.value = new BigDecimal(value);
            }
        }
    }

    public boolean isOperation() {
        return operationType.getValue() != 4;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QueryElement that = (QueryElement) o;

        if (operationType != that.operationType) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (operationType != null ? operationType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "QueryElement{" +
                "value=" + value +
                ", operationType=" + operationType +
                '}';
    }
}
