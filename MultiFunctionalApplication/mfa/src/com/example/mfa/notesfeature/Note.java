/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa.notesfeature;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Note implements Parcelable {

    private int id;

    private String value;

    private String description;

    private Date lastEditDate;

    private Importance importance;

    private String imagePath;

    public String getImagePath2() {
        return imagePath2;
    }

    public void setImagePath2(String imagePath2) {
        this.imagePath2 = imagePath2;
    }

    private String imagePath2;

    public Note() {
    }

    public Note(int id, String value, Date lastEditDate, Importance importance, String description, String imagePath, String imagePath2) {
        this.id = id;
        this.imagePath2 = imagePath2;
        this.value = value;
        this.lastEditDate = lastEditDate;
        this.importance = importance;
        this.description = description;
        this.imagePath = imagePath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getLastEditDate() {
        return lastEditDate;
    }

    public void setLastEditDate(Date lastEditDate) {
        this.lastEditDate = lastEditDate;
    }

    public Importance getImportance() {
        return importance;
    }

    public void setImportance(Importance importance) {
        this.importance = importance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(value);
        dest.writeString(description);
        dest.writeSerializable(lastEditDate);
        dest.writeSerializable(importance);
        dest.writeString(imagePath);
        dest.writeString(imagePath2);
    }


    public static final Parcelable.Creator<Note> CREATOR = new Creator<Note>() {
        public Note createFromParcel(Parcel source) {
            Note note = new Note();
            note.setId(source.readInt());
            note.setValue(source.readString());
            note.setDescription(source.readString());
            note.setLastEditDate((Date) source.readSerializable());
            note.setImportance((Importance) source.readSerializable());
            note.setImagePath(source.readString());
            note.setImagePath2(source.readString());
            return note;
        }

        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

}
