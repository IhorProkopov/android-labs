package com.example.Weather;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ExecutionException;

public class MyActivity extends AppWidgetProvider {

    public static String ACTION_WIDGET_RECEIVER = "ActionReceiverWidget";

    public String teperature, fTeperature, windspeedKmph, humidity, deck;

    ComponentName cmName;
    ;


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        //Создаем новый RemoteViews
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.main);
        remoteViews.setTextViewText(R.id.temp_label,"Temperature: " + teperature+"("+fTeperature+")");
        remoteViews.setTextViewText(R.id.speed_label,"Wind speed: "+ windspeedKmph+"Kmph");
        remoteViews.setTextViewText(R.id.humidity_label, "Humidity: "+humidity+"%");
        remoteViews.setTextViewText(R.id.deck_label, deck);
        cmName = new ComponentName(context, MyActivity.class);
        appWidgetManager.updateAppWidget(cmName, remoteViews);
        //Подготавливаем Intent для Broadcast
        Intent active = new Intent(context, MyActivity.class);
        active.setAction(ACTION_WIDGET_RECEIVER);
        active.putExtra("msg", "Hello Habrahabr");

        //создаем наше событие
        PendingIntent actionPendingIntent = PendingIntent.getBroadcast(context, 0, active, 0);

        //регистрируем наше событие
        remoteViews.setOnClickPendingIntent(R.id.widget_button, actionPendingIntent);

        //обновляем виджет
        appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        String url = "http://api.worldweatheronline.com/free/v2/weather.ashx?q=Kharkiv&format=json&num_of_days=1&key=741df1a0af8aa051437cc0f9290a2";
        try {
            new JSONAsyncTask().execute(url).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.main);
        remoteViews.setTextViewText(R.id.temp_label,"Temperature: " + teperature+"("+fTeperature+")");
        remoteViews.setTextViewText(R.id.speed_label,"Wind speed: "+ windspeedKmph+"Kmph");
        remoteViews.setTextViewText(R.id.humidity_label, "Humidity: "+humidity+"%");
        remoteViews.setTextViewText(R.id.deck_label, deck);
        cmName = new ComponentName(context, MyActivity.class);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        appWidgetManager.updateAppWidget(cmName, remoteViews);
        //Ловим наш Broadcast, проверяем и выводим сообщение
        final String action = intent.getAction();
//        if (ACTION_WIDGET_RECEIVER.equals(action)) {

    }


    class JSONAsyncTask extends AsyncTask<String, Void, Boolean> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Boolean doInBackground(String... urls) {
            try {

                //------------------>>
                HttpGet httppost = new HttpGet(urls[0]);
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse response = httpclient.execute(httppost);

                // StatusLine stat = response.getStatusLine();
                int status = response.getStatusLine().getStatusCode();

                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);


                    JSONObject jsono = new JSONObject(data);
                    JSONObject object = jsono.getJSONObject("data").getJSONArray("current_condition").getJSONObject(0);
                    teperature = object.get("temp_C").toString();
                    fTeperature = object.get("FeelsLikeC").toString();
                    windspeedKmph = object.get("windspeedKmph").toString();
                    humidity = object.get("humidity").toString();
                    deck = object.getJSONArray("weatherDesc").getJSONObject(0).get("value").toString();
                    Log.d("Responce", jsono.toString());
                    return true;
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {

                e.printStackTrace();
            }
            return false;
        }

        protected void onPostExecute(Boolean result) {

        }
    }


}
