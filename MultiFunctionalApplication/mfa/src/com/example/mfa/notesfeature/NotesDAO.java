/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa.notesfeature;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class NotesDAO {
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    private static final String TABLE_NAME = "notes";

    private DBHelper dbHelper;

    public NotesDAO(Context context) {
        this.dbHelper = new DBHelper(context);
        dbHelper.onCreate(dbHelper.getWritableDatabase());
    }

    public void add(Note note) {
        ContentValues cv = getContentValues(note);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.insert(TABLE_NAME, null, cv);
        db.close();
    }

    public void update(Note note) {
        ContentValues cv = getContentValues(note);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.update(TABLE_NAME, cv, "id=?", new String[]{String.valueOf(note.getId())});
    }

    public Note getNote(int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(TABLE_NAME, null, "id=?", new String[]{String.valueOf(id)}, null, null, null);
        List<Note> notes = readAllNotesFromCursor(c);
        return notes.size() > 0 ? notes.get(0) : null;
    }

    public int getCount() {
        return getAll().length;
    }

    public Note[] getByImportance(Importance importance) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(TABLE_NAME, null, "importance=?", new String[]{importance.getValue()}, null, null, null);
        List<Note> notes = readAllNotesFromCursor(c);
        db.close();
        return notes.toArray(new Note[notes.size()]);
    }

    public Note[] getByText(String query) {
        if (query.isEmpty()) {
            return getAll();
        }
        String selectionArg = "%" + query + "%";
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(TABLE_NAME, null, "value LIKE ? OR description LIKE ? ", new String[]{selectionArg, selectionArg}, null, null, null);
        List<Note> notes = readAllNotesFromCursor(c);
        db.close();
        return notes.toArray(new Note[notes.size()]);
    }

    public Note[] getAll() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(TABLE_NAME, null, null, null, null, null, null);
        List<Note> notes = readAllNotesFromCursor(c);
        db.close();
        return notes.toArray(new Note[notes.size()]);
    }

    public int delete(int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int res = db.delete("notes", "id=" + id, null);
        db.close();
        return res;
    }

    private ContentValues getContentValues(Note note) {
        ContentValues cv = new ContentValues();
        cv.put("value", note.getValue());
        cv.put("description", note.getDescription());
        cv.put("lastEditDate", SIMPLE_DATE_FORMAT.format(note.getLastEditDate()));
        cv.put("importance", note.getImportance().getValue());
        cv.put("imagePath", note.getImagePath());
        cv.put("imagePath2", note.getImagePath2());
        return cv;
    }

    private List<Note> readAllNotesFromCursor(Cursor c) {
        List<Note> notes = new ArrayList<Note>();
        if (c.moveToFirst()) {
            Note n;
            try {
                n = new Note(c.getInt(c.getColumnIndex("id")), c.getString(c.getColumnIndex("value")), SIMPLE_DATE_FORMAT.parse(c.getString(c.getColumnIndex("lastEditDate"))), Importance.getImportanceByValue(c.getString(c.getColumnIndex("importance"))), c.getString(c.getColumnIndex("description")), c.getString(c.getColumnIndex("imagePath")), c.getString(c.getColumnIndex("imagePath2")));
                notes.add(n);
            } catch (Exception e) {
                Log.e("Smth Wrong((((", "PIZDNETC");
                Log.e("Wrong message", e.getMessage());
            }

            while (c.moveToNext()) {
                try {
                    n = new Note(c.getInt(c.getColumnIndex("id")), c.getString(c.getColumnIndex("value")), SIMPLE_DATE_FORMAT.parse(c.getString(c.getColumnIndex("lastEditDate"))), Importance.getImportanceByValue(c.getString(c.getColumnIndex("importance"))), c.getString(c.getColumnIndex("description")), c.getString(c.getColumnIndex("imagePath")), c.getString(c.getColumnIndex("imagePath2")));
                    notes.add(n);
                } catch (Exception e) {
                    Log.e("Smth Wrong((((", "PIZDNETC");
                    Log.e("Wrong message", e.getMessage());
                }
            }
        }
        return notes;
    }

}
