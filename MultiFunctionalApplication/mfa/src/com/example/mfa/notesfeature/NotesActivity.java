/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa.notesfeature;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.*;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.mfa.ActivityStyle;
import com.example.mfa.ActivityUtils;
import com.example.mfa.R;

import java.util.List;

public class NotesActivity extends ListActivity implements SearchView.OnCloseListener, SearchView.OnQueryTextListener {
    private NotesDAO notesDAO;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notes_page);
        notesDAO = new NotesDAO(this);
        getAllNotes();

        ViewGroup layout = (ViewGroup) findViewById(R.id.notes_page_root_layout);
        ActivityUtils.setStyleToTextView(ActivityStyle.getInstance(), layout, getAssets());
        if (ActivityStyle.getInstance().getBackgroungID() != 0)
            layout.setBackgroundResource(ActivityStyle.getInstance().getBackgroungID());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        setupSearchView();
        return (super.onCreateOptionsMenu(menu));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.addNote:
                Intent myIntent = new Intent(NotesActivity.this, AddNoteActivity.class);
                Bundle bundle = new Bundle();
                bundle.putBoolean("isCreating", true);
                myIntent.putExtras(bundle);
                startActivityForResult(myIntent, 0);
                return true;
            case R.id.all:
                getAllNotes();
                return true;
            case R.id.not_important:
                getNotesByImportance(Importance.NOT_IMPORTANT);
                return true;
            case R.id.important:
                getNotesByImportance(Importance.IMPORTANT);
                return true;
            case R.id.very_important:
                getNotesByImportance(Importance.VERY_IMPORTANT);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        recreate();
    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        final Note item = (Note) getListAdapter().getItem(position);
        Toast toast = Toast.makeText(getApplicationContext(), item.getValue(), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.notes_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        final Note note = (Note) getListAdapter().getItem(info.position);
        switch (item.getItemId()) {
            case R.id.editNote:
                Intent myIntent = new Intent(NotesActivity.this, AddNoteActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("noteID", note.getId());
                bundle.putBoolean("isCreating", false);
                myIntent.putExtras(bundle);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(myIntent, 0);
                break;
            case R.id.deleteNote:
                int deleted = notesDAO.delete(note.getId());
                Toast.makeText(this, "Delete rows: " + deleted, Toast.LENGTH_SHORT).show();
                getAllNotes();
                break;
        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        new AsyncDB().execute(new String[]{query});
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private void getNotesByImportance(Importance importance) {
        new AsyncDB().execute(new Importance[]{importance});
        TextView tv = (TextView) findViewById(R.id.notes_count);
        tv.setText(notesDAO.getByImportance(importance).length + "/" + notesDAO.getCount());
    }

    private void getAllNotes() {
        TextView tv = (TextView) findViewById(R.id.notes_count);
        tv.setText(notesDAO.getCount() + "/" + notesDAO.getCount());
        new AsyncDB().execute();
    }

    private void populateListView(Note[] notes) {
        final ListView listview = getListView();
        registerForContextMenu(getListView());
        final NotesAdapter adapter = new NotesAdapter(this,
                android.R.layout.simple_list_item_1, notes);
        listview.setAdapter(adapter);
        setListAdapter(adapter);
    }

    private void setupSearchView() {

        searchView.setIconifiedByDefault(true);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) {
            List<SearchableInfo> searchables = searchManager.getSearchablesInGlobalSearch();

            // Try to use the "applications" global search provider
            SearchableInfo info = searchManager.getSearchableInfo(getComponentName());
            for (SearchableInfo inf : searchables) {
                if (inf.getSuggestAuthority() != null
                        && inf.getSuggestAuthority().startsWith("applications")) {
                    info = inf;
                }
            }
            searchView.setSearchableInfo(info);
        }

        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
    }


    @Override
    public boolean onClose() {
        getAllNotes();
        return false;
    }

    private class AsyncDB extends AsyncTask<Object, Void, Void> {

        ProgressDialog progressDialog;
        Note[] notes;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(NotesActivity.this, "Wait", "Updating...");
        }

        @Override
        protected Void doInBackground(Object... params) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (params.length > 0) {
                if (params[0] instanceof String) {
                    notes = notesDAO.getByText((String) params[0]);
                } else {
                    if (params[0] instanceof Importance) {
                        notes = notesDAO.getByImportance((Importance) params[0]);
                    }
                }
            } else {
                notes = notesDAO.getAll();
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            populateListView(notes);
            progressDialog.dismiss();
        }

    }


}
