/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa.calculatorfeature;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.mfa.ActivityStyle;
import com.example.mfa.ActivityUtils;
import com.example.mfa.R;

public class CalculatorActivity extends Activity implements View.OnClickListener {
    private Button one, two, three, four, five, six, seven, eight, nine, zero, add, sub, mul, div, clean, equal, backspace, dot;
    private TextView disp;
    CalclatorService cs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculator_page);
        ViewGroup layout = (ViewGroup) findViewById(R.id.calculator_root_layout);
        ActivityUtils.setStyleToTextView(ActivityStyle.getInstance(), layout, getAssets());
        //(LinearLayout)findViewById(R.id.)
        if (ActivityStyle.getInstance().getBackgroungID() != 0)
            layout.setBackgroundResource(ActivityStyle.getInstance().getBackgroungID());
        one = (Button) findViewById(R.id.oneButton);
        two = (Button) findViewById(R.id.twoButton);
        three = (Button) findViewById(R.id.threeButton);
        four = (Button) findViewById(R.id.fourButton);
        five = (Button) findViewById(R.id.fiveButton);
        six = (Button) findViewById(R.id.sixButton);
        seven = (Button) findViewById(R.id.sevenButton);
        eight = (Button) findViewById(R.id.eightButton);
        nine = (Button) findViewById(R.id.nineButton);
        zero = (Button) findViewById(R.id.zeroButton);
        add = (Button) findViewById(R.id.plusButton);
        sub = (Button) findViewById(R.id.minusButton);
        mul = (Button) findViewById(R.id.multiplyButton);
        div = (Button) findViewById(R.id.divideButton);
        clean = (Button) findViewById(R.id.cleanButton);
        equal = (Button) findViewById(R.id.equalsButton);
        backspace = (Button) findViewById(R.id.deleteButton);
        dot = (Button) findViewById(R.id.dotButton);
        disp = (TextView) findViewById(R.id.calculatorWindow);
        disp.setMovementMethod(ScrollingMovementMethod.getInstance());

        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        four.setOnClickListener(this);
        five.setOnClickListener(this);
        six.setOnClickListener(this);
        seven.setOnClickListener(this);
        eight.setOnClickListener(this);
        nine.setOnClickListener(this);
        zero.setOnClickListener(this);
        clean.setOnClickListener(this);
        add.setOnClickListener(this);
        sub.setOnClickListener(this);
        mul.setOnClickListener(this);
        div.setOnClickListener(this);
        equal.setOnClickListener(this);
        backspace.setOnClickListener(this);
        dot.setOnClickListener(this);

        cs = new CalclatorService(getResources().getConfiguration().locale);

    }

    @Override
    public void onClick(View arg0) {
        String currentText = disp.getText().toString();
        switch (arg0.getId()) {
            case R.id.oneButton:
                currentText = cs.numberPressed(currentText, "1");
                break;
            case R.id.twoButton:
                currentText = cs.numberPressed(currentText, "2");
                break;
            case R.id.threeButton:
                currentText = cs.numberPressed(currentText, "3");
                break;
            case R.id.fourButton:
                currentText = cs.numberPressed(currentText, "4");
                break;
            case R.id.fiveButton:
                currentText = cs.numberPressed(currentText, "5");
                break;
            case R.id.sixButton:
                currentText = cs.numberPressed(currentText, "6");
                break;
            case R.id.sevenButton:
                currentText = cs.numberPressed(currentText, "7");
                break;
            case R.id.eightButton:
                currentText = cs.numberPressed(currentText, "8");
                break;
            case R.id.nineButton:
                currentText = cs.numberPressed(currentText, "9");
                break;
            case R.id.zeroButton:
                currentText = cs.numberPressed(currentText, "0");
                break;
            case R.id.dotButton:
                currentText = cs.dotPressed(currentText);
                break;
            case R.id.equalsButton:
                try {
                    currentText = cs.equalsPressed(currentText).toString();
                } catch (QueryElementException e) {
                    currentText = "";
                    Toast.makeText(getApplicationContext(), "Cannot divide by zero!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.plusButton:
                currentText = cs.operationPressed(currentText, "+");
                break;
            case R.id.minusButton:
                currentText = cs.operationPressed(currentText, "-");
                break;
            case R.id.multiplyButton:
                currentText = cs.operationPressed(currentText, "*");
                break;
            case R.id.divideButton:
                currentText = cs.operationPressed(currentText, "/");
                break;
            case R.id.cleanButton:
                currentText = cs.clearPressed();
                break;
            case R.id.deleteButton:
                currentText = cs.deletePressed(currentText);
                break;
        }
        disp.setText(currentText);
    }

}



