package com.example.mfa.notesfeature;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {

    private static final String TAG = "Notification";
    Intent intent;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "On receive retrieved");
        Intent service1 = new Intent(context, AlarmService.class);
        Bundle mBundle = new Bundle();
        mBundle.putInt("noteID", intent.getIntExtra("noteID", -1));
        service1.putExtras(mBundle);
        context.startService(service1);
        Log.i(TAG, "On receive finish");
    }

}
