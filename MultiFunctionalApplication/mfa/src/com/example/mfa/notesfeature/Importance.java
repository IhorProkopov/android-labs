package com.example.mfa.notesfeature;

import android.graphics.Color;

import java.io.Serializable;

public enum Importance implements Serializable {

    VERY_IMPORTANT(Color.rgb(255, 79, 21), "Very Important"), IMPORTANT(Color.rgb(255, 206, 38), "Important"), NOT_IMPORTANT(Color.rgb(35, 255, 71), "Not important");

    public int getColour() {
        return colour;
    }

    public String getValue() {
        return value;
    }

    private int colour;
    private String value;

    private Importance(int colour, String value) {
        this.colour = colour;
        this.value = value;
    }

    public static Importance getImportanceByValue(String value) {
        Importance[] values = Importance.values();
        for (Importance i : values) {
            if (i.getValue().equals(value)) {
                return i;
            }
        }
        return null;
    }

}
