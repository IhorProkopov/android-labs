/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa.calculatorfeature;

public class QueryElementException extends Exception {

    public QueryElementException() {
        super();
    }

    public QueryElementException(String message) {
        super(message);
    }

}
