/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa.notesfeature;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mfa.ActivityStyle;
import com.example.mfa.ActivityUtils;
import com.example.mfa.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;

public class NotesAdapter extends ArrayAdapter<Note> {

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    private Context context;
    private Note[] notes;

    public NotesAdapter(Context context, int resource, Note[] objects) {
        super(context, resource, objects);
        this.context = context;
        this.notes = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Note currentNote = notes[position];
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.note, parent, false);
        TextView description = (TextView) rowView.findViewById(R.id.noteDescription);
        TextView lastModified = (TextView) rowView.findViewById(R.id.creationTime);
        ImageView importance = (ImageView) rowView.findViewById(R.id.importance);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        ImageView imageView2 = (ImageView) rowView.findViewById(R.id.icon2);
        if (currentNote.getImportance() != null) {
            switch (currentNote.getImportance()) {
                case IMPORTANT:
                    importance.setBackgroundColor(Importance.IMPORTANT.getColour());
                    break;
                case VERY_IMPORTANT:
                    importance.setBackgroundColor(Importance.VERY_IMPORTANT.getColour());
                    break;
                case NOT_IMPORTANT:
                    importance.setBackgroundColor(Importance.NOT_IMPORTANT.getColour());
                    break;
            }
        } else {
            importance.setBackgroundColor(Color.rgb(255, 255, 255));
        }
        description.setText(currentNote.getDescription());
        lastModified.setText(SIMPLE_DATE_FORMAT.format(currentNote.getLastEditDate()));
        imageView.setImageResource(R.drawable.ic_launcher);
        if (currentNote.getImagePath() != null) {
            Uri uriFromPath = Uri.fromFile(new File(currentNote.getImagePath()));
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(uriFromPath));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            imageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 60, 60, false));
        }
        if (currentNote.getImagePath2() != null) {
            Uri uriFromPath = Uri.fromFile(new File(currentNote.getImagePath2()));
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(uriFromPath));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            imageView2.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 60, 60, false));
        }
        ActivityUtils.setStyleToTextView(ActivityStyle.getInstance(), (ViewGroup) rowView, getContext().getAssets());
        return rowView;
    }

}
