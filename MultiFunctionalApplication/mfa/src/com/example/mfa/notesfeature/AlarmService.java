package com.example.mfa.notesfeature;


import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.example.mfa.R;


public class AlarmService extends IntentService {

    private static final int NOTIFICATION_ID = 1;
    private static final String TAG = "Notification";
    private NotificationManager notificationManager;
    private PendingIntent pendingIntent;
    private NotesDAO notesDAO;


    public AlarmService() {
        super("AlarmService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notesDAO = new NotesDAO(getApplicationContext());
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int noteID = intent.getExtras().getInt("noteID");
        Note note = notesDAO.getNote(noteID);
        Log.i(TAG, "Alarm Service has started.");
        Context context = this.getApplicationContext();
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent mIntent = new Intent(this, AddNoteActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("noteID", noteID);
        bundle.putBoolean("isCreating", false);
        mIntent.putExtras(bundle);
        pendingIntent = PendingIntent.getActivity(context, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Resources res = this.getResources();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.ic_launcher))
                .setTicker(note.getDescription())
                .setAutoCancel(true)
                .setContentTitle(note.getDescription())
                .setContentText(note.getValue());

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
        Log.i(TAG, "Notifications sent.");
    }

}