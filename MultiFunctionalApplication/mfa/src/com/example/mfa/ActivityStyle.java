/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa;

public class ActivityStyle {

    private static final ActivityStyle INSTANCE = new ActivityStyle();
    private String pathToTTF;
    private int color;
    private int backgroungID;

    private ActivityStyle() {
    }


    public static ActivityStyle getInstance() {
        return INSTANCE;
    }

    public String getPathToTTF() {
        return pathToTTF;//"fonts/1942.ttf";//pathToTTF;
    }

    public void setPathToTTF(String pathToTTF) {
        this.pathToTTF = pathToTTF;
    }


    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getBackgroungID() {
        return backgroungID;
    }

    public void setBackgroungID(int backgroungID) {
        this.backgroungID = backgroungID;
    }
}
