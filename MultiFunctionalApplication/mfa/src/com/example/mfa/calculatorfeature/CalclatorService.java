/**
 * Copyright 2014 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.example.mfa.calculatorfeature;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

public class CalclatorService {

    private Locale locale;
    private Calculator calculator;

    public CalclatorService(Locale locale) {
        this.locale = locale;
        calculator = new Calculator(locale);
    }

    public String numberPressed(String actualText, String number) {
        return actualText + number;
    }

    public String clearPressed() {
        return "";
    }

    public String dotPressed(String actualText) {
        if (!actualText.isEmpty()) {
            char lastCharacter = actualText.charAt(actualText.length() - 1);
            if (lastCharacter == '-' || lastCharacter == '+' || lastCharacter == '*' || lastCharacter == '/') {
                return actualText + ".";
            }
            if (lastCharacter == '.') {
                return actualText;
            }
            List<QueryElement> input = calculator.convertQuery(actualText);
            String lastNumber = input.get(input.size() - 1).getValue().toString();
            if (!lastNumber.contains(".")) {
                return actualText + ".";
            } else {
                return actualText;
            }
        } else {
            return ".";
        }
    }

    public String operationPressed(String actualText, String operation) {
        if (actualText.length() > 0) {
            String lastCharacter = actualText.substring(actualText.length() - 1, actualText.length());
            if (isDigit(lastCharacter) || lastCharacter.equals(".")) {
                return actualText + operation;
            } else {
                return actualText.substring(0, actualText.length() - 1) + operation;
            }
        }
        return "";
    }

    public String deletePressed(String actualText) {
        if (actualText.length() > 0) {
            return actualText.substring(0, actualText.length() - 1);
        }
        return "";
    }

    public BigDecimal equalsPressed(String actualText) throws QueryElementException {
        if (actualText.length() == 0) {
            return new BigDecimal("0");
        }
        return calculator.calculate(actualText);
    }

    private boolean isDigit(String value) {
        return value.equals("0") || value.equals("1") || value.equals("2") || value.equals("3") || value.equals("4") || value.equals("5") || value.equals("6") || value.equals("7") || value.equals("8") || value.equals("9");
    }

//    public static void main(String[] args)
//    {
////        String s = ".";
////        System.out.println(s);
////        System.out.println(s.equals("."));
////        System.out.println(s.equals("\\."));
////        CalclatorService c = new CalclatorService();
////        System.out.println(c.dotPressed(""));
////        System.out.println(c.dotPressed("2345+2345"));
////        System.out.println(c.dotPressed("654+654+"));
////        System.out.println(c.dotPressed("."));
////        System.out.println(c.dotPressed("654+54.4"));
////        System.out.println(c.dotPressed("1234."));
//    }

}
